package cases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Forms {
    WebDriver driver;

    public Forms(WebDriver driver) {this.driver = driver;}

    private final By FORMS = By.xpath("//h5[contains(text(),'Forms')]");
    private final By PRACTICEFORM = By.xpath("//span[contains(text(),'Practice Form')]");
    private final By FIRSTNAME = By.id("firstName");
    private final By LASTNAME = By.id("lastName");
    private final By GENDER = By.xpath("//label[contains(text(),'Female')]");
    private final By MOBILE = By.id("userNumber");
    private final By SUBMIT = By.id("submit");
    private final By CLOSELARGEMODAL = By.id("closeLargeModal");

    public void clickForms() {driver.findElement(FORMS).click();}

    public void clickPracticeForm() {driver.findElement(PRACTICEFORM).click();}

    public void fillFirstName(String text) {
        driver.findElement(FIRSTNAME).sendKeys(text);
    }

    public void fillLastName(String text) {
        driver.findElement(LASTNAME).sendKeys(text);
    }

    public void clickGender() {driver.findElement(GENDER).click();}

    public void fillMobile(Object mobileChars) {
        driver.findElement(MOBILE).sendKeys((CharSequence) mobileChars);
    }

    public void clickSubmit() {driver.findElement(SUBMIT).click();}

    public Forms submitSuccesful(String firstName, String lastName, Object mobile) {
        this.fillFirstName(firstName);
        this.fillLastName(lastName);
        this.fillMobile(mobile);
        return new Forms(driver);
    }

    public void clickClose() {driver.findElement(CLOSELARGEMODAL).click();}

}
