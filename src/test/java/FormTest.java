import cases.Forms;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class FormTest {

    private WebDriver driver;
    private Forms forms;

    @BeforeTest
    public void setUP(){
        System.setProperty("webdriver.chrome.driver", "D:\\Drivers_ets\\chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String url = "https://demoqa.com";
        driver.get(url);
        forms = new Forms(driver);
        forms.clickForms();
        forms.clickPracticeForm();
    }

    @Test
    public void confirmationSuccessfullySubmittedForm() {
        forms.submitSuccesful("Test First Name", "Test Last Name", "80682241683");
        forms.clickGender();
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)", "");
        forms.clickSubmit();

        String info = driver.findElement(By.id("example-modal-sizes-title-lg")).getText();
        Assert.assertEquals("Thanks for submitting the form", info);

        forms.clickClose();
    }

    @Test
    public void firstNameRequired() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,250)", "");
        forms.clickSubmit();

        WebElement firstName = driver.findElement(By.id("firstName"));
        Assert.assertTrue(Boolean.parseBoolean(firstName.getAttribute("required")), "first name is required");
    }

    @Test
    public void lastNameRequired() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,250)", "");
        forms.clickSubmit();

        WebElement lastName = driver.findElement(By.id("lastName"));
        Assert.assertTrue(Boolean.parseBoolean(lastName.getAttribute("required")), "last name is required");
    }

    @Test
    public void maleRequired() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,250)", "");
        forms.clickSubmit();

        WebElement male = driver.findElement(By.id("gender-radio-1"));
        Assert.assertTrue(Boolean.parseBoolean(male.getAttribute("required")), "male is required");
    }

    @Test
    public void femaleRequired() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,250)", "");
        forms.clickSubmit();

        WebElement female = driver.findElement(By.id("gender-radio-2"));
        Assert.assertTrue(Boolean.parseBoolean(female.getAttribute("required")), "female is required");
    }

    @Test
    public void otherRequired() {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,250)", "");
        forms.clickSubmit();

        WebElement other = driver.findElement(By.id("gender-radio-3"));
        Assert.assertTrue(Boolean.parseBoolean(other.getAttribute("required")), "other is required");
    }

    @AfterTest
    public void close() {driver.close();}

}
